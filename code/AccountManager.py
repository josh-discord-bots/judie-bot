import discord
from discord import Embed, File
from discord.ext import commands, tasks
from discord.ext.commands import cooldown, BucketType
import random
import sqlite3
from Utilities import HelperClass


class AccountManager(commands.Cog):
    def __init__(self, client):
        self.client = client
        self.helperClass = HelperClass()

    msgID = ""
    deleteMsg = ""
    userThatWantsToDelete = ""

    # HELPER FUNCTIONS

    async def getUserID(self, discordID, cursor):

        cursor.execute("SELECT user_id FROM users WHERE discord_id=?", [discordID])
        uID = cursor.fetchone()
        if uID is not None:
            return uID[0]
        else:
            return uID

    async def checkUser(self, discord_id: str, cursor):

        cursor.execute("SELECT user_id FROM users WHERE discord_id = ?", [discord_id])
        users = cursor.fetchone()

        cursor.execute("SELECT user_id FROM usersnew WHERE discord_id = ?", [discord_id])
        usersnew = cursor.fetchone()

        if users is None and usersnew is None:
            return "register"
        elif users is None and usersnew is not None:
            return "update"
        else:
            return "good"

    # COMMANDS & RELATED

    @commands.command()
    async def register(self, ctx):
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()
        discordID = str(ctx.author.id)

        title = ""
        text = ""
        footer = ""

        userCheck = await self.checkUser(discord_id=discordID, cursor=cursor)
        # check user registration --> checkUser()
        # if good send error message --> good to go
        if userCheck != "register":
            title = f"{str(ctx.author)[:-5]} is already registered."
            text = "If you can't access the games, try updating (-update)!"
            footer = "Please enjoy!"
        # if register:
        else:
            cursor.execute("INSERT INTO users (discord_id) VALUES (?)", [discordID])
            uID = await self.getUserID(discordID, cursor)
            cursor.execute("INSERT INTO oialt (user_id) VALUES (?)", [uID])
            cursor.execute("INSERT INTO oialt_harem (user_id) VALUES (?)", [uID])
            cursor.execute("INSERT INTO stabby_mikes (user_id) VALUES (?)", [uID])
            cursor.execute("INSERT INTO the_boys (user_id) VALUES (?)", [uID])
            cursor.execute("INSERT INTO li_potential (user_id) VALUES (?)", [uID])
            cursor.execute("INSERT INTO eternum (user_id) VALUES (?)", [uID])
            cursor.execute("INSERT INTO eternum_harem (user_id) VALUES (?)", [uID])
            cursor.execute("INSERT INTO homies (user_id) VALUES (?)", [uID])
            cursor.execute("INSERT INTO side_girls (user_id) VALUES (?)", [uID])
            cursor.execute("INSERT INTO creatures (user_id) VALUES (?)", [uID])

            title = "Great Success!"
            text = f"user {ctx.author.mention} was successfully registered to the database!"
            footer = "Welcome aboard!"
            db.commit()

        embed = await HelperClass.createEmbed(self=self.helperClass, title=title, text=text, footer=footer)
        await ctx.send(embed=embed)

    @commands.command()
    async def deleteacc(self, ctx):
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()
        discordID = str(ctx.author.id)
        uid = await self.getUserID(discordID=discordID, cursor=cursor)
        user_name = str(ctx.author)

        if uid is not None:
            msg = await ctx.send("Are you sure you want to delete **all** your data?"
                                 "\nThis action is irreversible! (React to the checkmark to confirm.)")
            self.deleteMsg = ctx.message
            self.msgID = msg.id
            self.userThatWantsToDelete = ctx.author
            await msg.add_reaction('✅')
        else:
            cursor.execute("SELECT user_id FROM usersnew WHERE discord_id=?", [discordID])
            oldID = cursor.fetchone()
            if oldID is not None:
                msg = await ctx.send("Are you sure you want to delete **all** your data?"
                                     "\nThis action is irreversible! (React to the checkmark to confirm.)")
                self.deleteMsg = ctx.message
                self.msgID = msg.id
                self.userThatWantsToDelete = ctx.author
                await msg.add_reaction('✅')
            else:
                await ctx.send(f"User {user_name[:-5]} is not in our database I'm afraid...")
        db.commit()

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()
        discordID = str(self.deleteMsg.author.id)
        uid = await self.getUserID(discordID=discordID, cursor=cursor)
        oldID = None
        if uid is None:
            cursor.execute("SELECT user_id FROM usersnew WHERE discord_id = ?", [discordID])
            oldIDList = cursor.fetchone()
            oldID = oldIDList[0]
        user_name = str(self.deleteMsg.author)
        channel = self.client.get_channel(self.deleteMsg.channel.id)

        if self.msgID == payload.message_id:
            if self.userThatWantsToDelete.id == payload.user_id:
                if uid is not None:
                    cursor.execute("DELETE FROM users WHERE user_id = ?", [uid])
                    cursor.execute("DELETE FROM oialt WHERE user_id=?", [uid])
                    cursor.execute("DELETE FROM oialt_harem WHERE user_id = ?", [uid])
                    cursor.execute("DELETE FROM stabby_mikes WHERE user_id = ?", [uid])
                    cursor.execute("DELETE FROM the_boys WHERE user_id = ?", [uid])
                    cursor.execute("DELETE FROM li_potential WHERE user_id = ?", [uid])
                    cursor.execute("DELETE FROM eternum WHERE user_id=?", [uid])
                    cursor.execute("DELETE FROM eternum_harem WHERE user_id=?", [uid])
                    cursor.execute("DELETE FROM homies WHERE user_id=?", [uid])
                    cursor.execute("DELETE FROM side_girls WHERE user_id=?", [uid])
                    cursor.execute("DELETE FROM creatures WHERE user_id=?", [uid])
                else:
                    cursor.execute("DELETE FROM usersnew WHERE user_id=?", [oldID])
                    cursor.execute("DELETE FROM harem_new WHERE user_id=?", [oldID])
                    cursor.execute("DELETE FROM stabby_clan WHERE user_id=?", [oldID])
                    cursor.execute("DELETE FROM the_boys_new WHERE user_id=?", [oldID])
                    cursor.execute("DELETE FROM li_potential_new WHERE user_id=?", [oldID])

                await channel.send(f"User {user_name[:-5]} has been successfully removed from the database."
                                   f"Have a great time! :wave:")
                await self.deleteMsg.add_reaction('✅')

                db.commit()

                self.deleteMsg = ""
                self.userThatWantsToDelete = ""
                self.msgID = ""

    @commands.command()
    async def update(self, ctx):
        db = sqlite3.connect("main.sqlite")
        cursor = db.cursor()
        discordID = str(ctx.author.id)
        user_name = str(ctx.author)

        user = await self.checkUser(discordID, cursor)

        title = ""
        description = ""
        footer = ""

        if user == 'good':
            title = f"No update needed..."
            description = f"User {user_name} is already up to date!"
            footer = "Enjoy the gf game!"
        elif user == 'update':
            # something probably wrong here; uID returns None...
            cursor.execute("INSERT INTO users (discord_id) VALUES (?)", [(discordID)])
            uID = await self.getUserID(discordID=discordID, cursor=cursor)
            cursor.execute("SELECT user_id FROM usersnew WHERE discord_id = ?", [discordID])
            oldID = cursor.fetchone()[0]

            cursor.execute("SELECT funtime FROM usersnew WHERE user_id = ?", [oldID])
            funtime = cursor.fetchone()
            cursor.execute("SELECT mc FROM usersnew WHERE user_id = ?", [oldID])
            mc = cursor.fetchone()
            cursor.execute("SELECT aiko FROM usersnew WHERE user_id = ?", [oldID])
            aiko = cursor.fetchone()
            cursor.execute("SELECT nine_three FROM usersnew WHERE user_id = ?", [oldID])
            nine_three = cursor.fetchone()
            cursor.execute("SELECT last_gf FROM usersnew WHERE user_id = ?", [oldID])
            last_gf = cursor.fetchone()

            cursor.execute(
                "INSERT INTO oialt (user_id, funtime, mc, aiko, nine_three, last_gf) VALUES (?, ?, ?, ?, ?, ?)",
                [uID, funtime[0], mc[0], aiko[0], nine_three[0], last_gf[0]])

            # Port old tables to new:

            # Harem
            cursor.execute("SELECT judie FROM harem_new WHERE user_id = ?", [oldID])
            judie = cursor.fetchone()
            cursor.execute("SELECT lauren FROM harem_new WHERE user_id = ?", [oldID])
            lauren = cursor.fetchone()
            cursor.execute("SELECT messy_hair_lauren FROM harem_new WHERE user_id = ?", [oldID])
            mhlauren = cursor.fetchone()
            cursor.execute("SELECT carla FROM harem_new WHERE user_id = ?", [oldID])
            carla = cursor.fetchone()
            cursor.execute("SELECT iris FROM harem_new WHERE user_id = ?", [oldID])
            iris = cursor.fetchone()
            cursor.execute("SELECT aiko FROM harem_new WHERE user_id = ?", [oldID])
            aiko = cursor.fetchone()
            cursor.execute("SELECT jasmine FROM harem_new WHERE user_id = ?", [oldID])
            jasmine = cursor.fetchone()
            cursor.execute("SELECT rebecca FROM harem_new WHERE user_id = ?", [oldID])
            rebecca = cursor.fetchone()
            cursor.execute("SELECT last_li FROM harem_new WHERE user_id = ?", [oldID])
            lastli = cursor.fetchone()

            cursor.execute(
                "INSERT INTO oialt_harem (user_id, judie, lauren, messy_hair_lauren, carla, iris, aiko, jasmine, rebecca, last_li) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                [uID, judie[0], lauren[0], mhlauren[0], carla[0], iris[0], aiko[0], jasmine[0], rebecca[0], lastli[0]])

            # Stabby Clan
            cursor.execute("SELECT police FROM stabby_clan WHERE user_id = ?", [oldID])
            police = cursor.fetchone()
            cursor.execute("SELECT hitman FROM stabby_clan WHERE user_id = ?", [oldID])
            hitman = cursor.fetchone()
            cursor.execute("SELECT yakuza FROM stabby_clan WHERE user_id = ?", [oldID])
            yakuza = cursor.fetchone()
            cursor.execute("SELECT priest FROM stabby_clan WHERE user_id = ?", [oldID])
            priest = cursor.fetchone()
            cursor.execute("SELECT exterminator FROM stabby_clan WHERE user_id = ?", [oldID])
            exterminator = cursor.fetchone()
            cursor.execute("SELECT anastasia FROM stabby_clan WHERE user_id = ?", [oldID])
            anastasia = cursor.fetchone()
            cursor.execute("SELECT last_mike FROM stabby_clan WHERE user_id = ?", [oldID])
            last_mike = cursor.fetchone()

            cursor.execute(
                "INSERT INTO stabby_mikes (user_id, police, hitman, yakuza, priest, exterminator, anastasia, last_mike) VALUES (?,?,?,?,?,?,?,?)",
                [uID, police[0], hitman[0], yakuza[0], priest[0], exterminator[0], anastasia[0], last_mike[0]])

            # The Boys
            cursor.execute("SELECT mc FROM the_boys_new WHERE user_id = ?", [oldID])
            boysmc = cursor.fetchone()
            cursor.execute("SELECT tom FROM the_boys_new WHERE user_id = ?", [oldID])
            tom = cursor.fetchone()
            cursor.execute("SELECT oliver FROM the_boys_new WHERE user_id = ?", [oldID])
            oliver = cursor.fetchone()
            cursor.execute("SELECT fit_jack FROM the_boys_new WHERE user_id = ?", [oldID])
            fitjack = cursor.fetchone()
            cursor.execute("SELECT asmodeus FROM the_boys_new WHERE user_id = ?", [oldID])
            asmodeus = cursor.fetchone()
            cursor.execute("SELECT hiromi FROM the_boys_new WHERE user_id = ?", [oldID])
            hiromi = cursor.fetchone()
            cursor.execute("SELECT last_boi FROM the_boys_new WHERE user_id = ?", [oldID])
            lastboi = cursor.fetchone()

            cursor.execute(
                "INSERT INTO the_boys (user_id, mc, tom, oliver, fit_jack, asmodeus, hiromi, last_boi) VALUES (?,?,?,?,?,?,?,?)",
                [uID, boysmc[0], tom[0], oliver[0], fitjack[0], asmodeus[0], hiromi[0], lastboi[0]])

            # Potential LI's
            cursor.execute("SELECT ava FROM li_potential_new WHERE user_id = ?", [oldID])
            ava = cursor.fetchone()
            cursor.execute("SELECT lilith FROM li_potential_new WHERE user_id = ?", [oldID])
            lilith = cursor.fetchone()
            cursor.execute("SELECT fit_jack_groupie FROM li_potential_new WHERE user_id = ?", [oldID])
            jackgroupie = cursor.fetchone()
            cursor.execute("SELECT train_conductor FROM li_potential_new WHERE user_id = ?", [oldID])
            conductor = cursor.fetchone()
            cursor.execute("SELECT shop_girl FROM li_potential_new WHERE user_id = ?", [oldID])
            shopgirl = cursor.fetchone()
            cursor.execute("SELECT stone_elephant FROM li_potential_new WHERE user_id = ?", [oldID])
            elephant = cursor.fetchone()
            cursor.execute("SELECT last_potential_li FROM li_potential_new WHERE user_id = ?", [oldID])
            lastpotli = cursor.fetchone()

            cursor.execute(
                "INSERT INTO li_potential (user_id, ava, lilith, fit_jack_groupie, train_conductor, shop_girl, stone_elephant, last_potential_li) VALUES (?,?,?,?,?,?,?,?)",
                [uID, ava[0], lilith[0], jackgroupie[0], conductor[0], shopgirl[0], elephant[0], lastpotli[0]])

            # Update user ID in all other tables
            cursor.execute("DELETE FROM harem_new WHERE user_id = ?", [oldID])
            cursor.execute("DELETE FROM stabby_clan WHERE user_id = ?", [oldID])
            cursor.execute("DELETE FROM the_boys_new WHERE user_id = ?", [oldID])
            cursor.execute("DELETE FROM li_potential_new WHERE user_id = ?", [oldID])

            # register user to all eternum tables
            cursor.execute("INSERT INTO eternum (user_id) VALUES (?)", [uID])
            cursor.execute("INSERT INTO eternum_harem (user_id) VALUES (?)", [uID])
            cursor.execute("INSERT INTO homies (user_id) VALUES (?)", [uID])
            cursor.execute("INSERT INTO side_girls (user_id) VALUES (?)", [uID])
            cursor.execute("INSERT INTO creatures (user_id) VALUES (?)", [uID])

            # delete entry from old table
            cursor.execute("DELETE FROM usersnew WHERE discord_id = ?", [discordID])

            title = "Great Success!"
            description = "You've successfully updated to Judie v2.0!"
            footer = "Have fun!"
            await ctx.message.add_reaction('✅')
        else:
            # build error embed that user needs to register
            title = f"Error #404 - User {str(ctx.author)} not registered!"
            description = "Please register before playing! (-register)"
            footer = "Contact Eisritter#6969 if you encounter any issues!"

        embed = await HelperClass.createEmbed(self=self.helperClass, title=title, text=description, footer=footer)
        await ctx.send(embed=embed)

        db.commit()


def setup(client):
    client.add_cog(AccountManager(client))
