## Changelog Judie V1.3.2

**Uploaded 24. January 2022**

Mostly focused on bugfixes now, fixed a number of smaller issues:

### Changes (Issues fixed):

- Cooldown not resetting for users issuing the commands gf; register; gf in that order

- The image of Penny in underwear popping up when asking for an Eva lewd

- The error message for Nova not having any lewds yet displaying text for Luna

- The help command not having been updated to include the train conductor in the OiaLt section
